Usage
=====

###Installation

```
curl -s http://getcomposer.org/installer | php
php composer.phar install
php index.php // php index.php buy
```

with docker

```
docker build . -t "track-tick-test"
docker run track-tick-test
```

###Test

```
./vendor/bin/kahlan
```

### Why do you don't use inheritance here ?

At first look my first attempt was to do a simple `ConsoleClass` that inherit from `ElectronicItem`.
Cool and "Easy Peasy" right ?

!['st2-easypeasy'](https://media.giphy.com/media/xT9IgNzWyweRI8O9tC/giphy.gif)


That's remember me my previous life in PHP/OOP world.

Back to school where we teached us all this hierarchical stuff about a FerrariCarClass who inherits from a CarClass which is also extends itself to other things like a Vehicle Class.

```
You can easily replace `CardAndStuffClass` by `HumainOrAnimalClass` if you don't like car :p
```

It was cool to do that at school and you replicate that at work but business is not "science". After 9 months you know a lot better about your business case and you have to migrate a bunch of child class because now a car (an equivalent in code `CarClass`) doesn't use wheels anymore.

!['backtothefutur'](https://media.giphy.com/media/WT40jXYyhIcww/giphy.gif)

All of the pain caused by inheritance can be traced back to the fact that inheritance forces ‘is-a’ rather than ‘has-a’ relationships Inheritance is hard because you MUST do it right, if not all your code wrote in your ChildClass will suffer from that. If you don't refactor your code quickly that's could be a disaster :p

Use `implements` not `extends` !

if you think in behaviours and use roles for code reuse you will get a much easier and much more powerful code base. You can easily reuse code. And you can reuse code in a way that you probably never thought.
