<?php
use App\Electronic\Console;
use App\Electronic\ElectronicType;

describe('ConsoleSpec', function() {
    it('getType equal Console', function() {
        $controller = new Console();
        expect($controller->getType())->toEqual(ElectronicType::Console());
    });
});
