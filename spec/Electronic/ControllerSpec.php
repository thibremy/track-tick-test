<?php
use App\Electronic\Controller;
use App\Electronic\ControllerType;
use App\Electronic\ElectronicType;

describe('ControllerSpec', function() {
    it('create a remote controller', function() {
        $controller = new Controller(ControllerType::Remote());
        expect($controller->isRemote())->toBe(true);
        expect($controller->isWired())->toBe(false);
    });

    it('create a wired controller', function() {
        $controller = new Controller(ControllerType::Wired());
        expect($controller->isWired())->toBe(true);
        expect($controller->isRemote())->toBe(false);
    });

    it('label is getType', function() {
        $controller = new Controller(ControllerType::Wired());
        expect($controller->getType())->toEqual(ElectronicType::Controller());
    });
});
