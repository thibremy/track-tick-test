<?php
use App\Electronic\ExtraItems;
use App\Electronic\ElectronicItemInterface;
use App\Electronic\Television;

describe('ExtraItemsSpec', function() {
    it('create ', function() {
        $extra = new ExtraItems(0);
        $extra2 = new ExtraItems(10);
        expect($extra->getLimit())->toBe(0);
        expect($extra2->getLimit())->toBe(10);
    });

    it('by default limit is 0', function() {
        $extra = new ExtraItems(0);
        expect(function() use (&$extra) {
            $extra->append(new Television());
        })->toThrow();
    });

    it('can add item if <= limit', function() {
        $extra = new ExtraItems(5);
        $appendToLimit = function () use (&$extra) {
            $extra->append(new Television());
            $extra->append(new Television());
            $extra->append(new Television());
            $extra->append(new Television());
            $extra->append(new Television());
        };
        expect($appendToLimit)->not->toThrow();
    });

    it('cant add item if > limit', function() {
        $extra = new ExtraItems(5);
        $appendOffsetLimit = function () use (&$extra) {
            $extra->append(new Television());
            $extra->append(new Television());
            $extra->append(new Television());
            $extra->append(new Television());
            $extra->append(new Television());
            $extra->append(new Television());
        };
        expect($appendOffsetLimit)->toThrow();
    });

    it('can map', function() {
        $extra = new ExtraItems(5);

        $extra->append(new Television());
        $extra->append(new Television());
        $extra->append(new Television());
        $extra->append(new Television());
        $extra->append(new Television());

        $identity = function ($v) {
            return $v->getType();
        };

        expect($extra->map($identity))->toHaveLength(5);
        expect($extra->map($identity))->toEqual([
            'Television',
            'Television',
            'Television',
            'Television',
            'Television'
        ]);
    });

    it('rreset limit >= length', function() {
        $extra = new ExtraItems(0);

        $extra->setLimit(1);
        $extra->append(new Television());
        $extra->setLimit(5);

        $appendToLimit = function () use (&$extra) {
            $extra->append(new Television());
            $extra->append(new Television());
            $extra->append(new Television());
            $extra->append(new Television());
        };

        expect($appendToLimit)->not->toThrow();
    });

    it('reset limit < length', function() {
        $extra = new ExtraItems(5);

        $extra->append(new Television());
        $extra->append(new Television());
        $extra->append(new Television());
        $extra->append(new Television());
        $extra->append(new Television());

        expect(function () use (&$extra) {
            $extra->setLimit(3);
        })->toThrow();
    });
});
