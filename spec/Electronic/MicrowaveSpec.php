<?php
use App\Electronic\Microwave;
use App\Electronic\ElectronicType;

describe('MicrowaveSpec', function() {
    it('label is getType', function() {
        $microwave = new Microwave();
        expect($microwave->getType())->toEqual(ElectronicType::Microwave());
    });
});
