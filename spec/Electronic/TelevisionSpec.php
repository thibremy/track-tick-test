<?php
use App\Electronic\Television;
use App\Electronic\ElectronicType;

describe('TelevisionSpec', function() {
    it('label is getType', function() {
        $tv = new Television();
        expect($tv->getType())->toEqual(ElectronicType::Television());
    });
});
