<?php
use App\Invoice\Invoice;
use App\Invoice\InvoiceItem;
use App\Invoice\InvoiceItemTree;
use App\Electronic\ElectronicProduct;

describe('InvoiceSpec', function() {
    it('create with bad items', function() {
        $badCreate = function () use (&$extra) {
            new Invoice([1]);
        };
        expect($badCreate)->toThrow();
    });

    it('create with InvoiceItem', function() {
        $console = ElectronicProduct::Console();
        $invoice = new Invoice([
            new InvoiceItem('xbox', 100),
            new InvoiceItem($console, 100)
        ]);
        expect($invoice->total())->toBe(200);
    });

    it('basic scenario', function() {
        $console = ElectronicProduct::Console();
        $xbox = ElectronicProduct::Console();
        $tv1 = ElectronicProduct::Television();
        $tv2 = ElectronicProduct::Television();
        $microwave = ElectronicProduct::Microwave();

        $xbox->extras->append(ElectronicProduct::WiredController());
        $xbox->extras->append(ElectronicProduct::WiredController());
        $xbox->extras->append(ElectronicProduct::RemoteController());
        $xbox->extras->append(ElectronicProduct::RemoteController());
        $tv1->extras->append(ElectronicProduct::RemoteController());
        $tv1->extras->append(ElectronicProduct::RemoteController());
        $tv2->extras->append(ElectronicProduct::RemoteController());

        $createInvoiceItemForExtra = function ($price) {
            return function ($extra) use ($price) {
                return new InvoiceItem($extra, $price);
            };
        };

        $invoiceItems = array_merge([],
            [new InvoiceItem($xbox, 100)],
            $xbox->extras->map($createInvoiceItemForExtra(10)),
            [new InvoiceItem($tv1, 10)],
            $tv1->extras->map($createInvoiceItemForExtra(10)),
            [new InvoiceItem($tv2, 10)],
            $tv2->extras->map($createInvoiceItemForExtra(10)),
            [new InvoiceItem($microwave, 10)],
            $microwave->extras->map($createInvoiceItemForExtra(10))
        );

        $invoice = new Invoice($invoiceItems);
        expect($invoice->total())->toBe(200);
    });
});
