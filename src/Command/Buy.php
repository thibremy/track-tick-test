<?php
namespace App\Command;

use Cilex\Provider\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Electronic\ElectronicProduct;
use App\Invoice;
use MyCLabs\Enum\Enum;

class Price extends Enum
{
    const Xbox = 200;
    const XboxWiredController = 20;
    const XboxRemoteController = 40;
    const Tv1 = 175;
    const Tv2 = 125;
    const Microwave = 40;
    const TvRemoteController = 30;
}

class Buy extends Command
{
    private $xbox;
    private $tv1;
    private $tv2;
    private $microwave;

    public function __construct()
    {
        parent::__construct();
        $this->xbox = ElectronicProduct::Console();
        $this->tv1 = ElectronicProduct::Television();
        $this->tv2 = ElectronicProduct::Television();
        $this->microwave = ElectronicProduct::Microwave();

        $this->xbox->extras->append(ElectronicProduct::WiredController());
        $this->xbox->extras->append(ElectronicProduct::WiredController());
        $this->xbox->extras->append(ElectronicProduct::RemoteController());
        $this->xbox->extras->append(ElectronicProduct::RemoteController());
        $this->tv1->extras->append(ElectronicProduct::RemoteController());
        $this->tv1->extras->append(ElectronicProduct::RemoteController());
        $this->tv2->extras->append(ElectronicProduct::RemoteController());
    }

    public function buy() {
        $createInvoiceItemForTvController = function ($price) {
            return function ($extra) use ($price) {
                return new Invoice\InvoiceItem($extra, $price);
            };
        };

        $createInvoiceItemForXboxController = function ($priceWired, $priceRemote) {
            return function ($extra) use ($priceWired, $priceRemote) {
                $price = $extra->isRemote() ?  $priceRemote : $priceWired;
                return new Invoice\InvoiceItem($extra, $price);
            };
        };

        $invoiceItems = array_merge([],
            [new Invoice\InvoiceItem($this->xbox, Price::Xbox)],
            $this->xbox->extras->map($createInvoiceItemForXboxController(Price::XboxWiredController, Price::XboxRemoteController)),
            [new Invoice\InvoiceItem($this->tv1, Price::Tv1)],
            $this->tv1->extras->map($createInvoiceItemForTvController(Price::TvRemoteController)),
            [new Invoice\InvoiceItem($this->tv2, Price::Tv2)],
            $this->tv2->extras->map($createInvoiceItemForTvController(Price::TvRemoteController)),
            [new Invoice\InvoiceItem($this->microwave, Price::Microwave)]
        );

        return $invoice = new Invoice\Invoice($invoiceItems);
    }

    private function getXboxInvoice($invoice) {
        return $xboxInvoice = $invoice->filter(function($invoiceItem) {
            $product = $invoiceItem->getProduct();
            if ($product === $this->xbox) {
                return true;
            }

            return in_array($product, iterator_to_array($this->xbox->extras));
        });
    }

    protected function configure()
    {
        $this->setName("buy")
            ->setDescription("buy your electronics stuff");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $invoice = $this->buy();
        $presenter = new Invoice\InvoicePresenter($output);
        $presenter->display($invoice);
        $output->writeln("Total ".$invoice->total()."$");
        $output->writeln('');
        $xboxInvoice = $this->getXboxInvoice($invoice);
        $output->writeln("Bundle console + controllers ".$xboxInvoice->total()."$");
        $output->writeln('');
    }
}
