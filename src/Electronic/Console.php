<?php
namespace App\Electronic;
use App\Electronic\ElectronicType;

class Console implements ElectronicItemInterface
{
    public $extras;

    public function __construct()
    {
        $this->extras = new ExtraItems(4);
    }

    public function maxExtras($limit)
    {
        $this->extras->setLimit($limit);
    }

    public function getType()
    {
        return ElectronicType::Console();
    }
}
