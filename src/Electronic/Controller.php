<?php
namespace App\Electronic;
use App\Electronic\ElectronicItemInterface;

class Controller implements ElectronicItemInterface
{
    public $extras;

    public function __construct(ControllerType $type)
    {
        $this->extras = new ExtraItems(0);
        $this->type = $type;
    }

    public function isWired()
    {
        return $this->type == ControllerType::Wired();
    }

    public function isRemote()
    {
        return $this->type == ControllerType::Remote();
    }

    public function maxExtras($limit)
    {
        $this->extras->setLimit($limit);
    }

    public function getType()
    {
        return ElectronicType::Controller();
    }

    public static function Wired()
    {
        return new Controller(ControllerType::Wired());
    }

    public static function Remote()
    {
        return new Controller(ControllerType::Remote());
    }
}
