<?php
namespace App\Electronic;
use MyCLabs\Enum\Enum;

class ControllerType extends Enum
{
    const Wired = 'wired';
    const Remote = 'remote';
}
