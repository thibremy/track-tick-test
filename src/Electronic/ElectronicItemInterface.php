<?php
namespace App\Electronic;

interface ElectronicItemInterface
{
    public function getType();
    public function maxExtras($limit);
}
