<?php
namespace App\Electronic;

use \App\Electronic\Legacy\ElectronicItemLegacyConverterInterface;
use \App\Electronic\Legacy\ElectronicItem;

class ElectronicProduct
{
    public static function Console()
    {
        return new Console();
    }

    public static function WiredController()
    {
        return new Controller(ControllerType::Wired());
    }

    public static function RemoteController()
    {
        return new Controller(ControllerType::Remote());
    }

    public static function Microwave()
    {
        return new Microwave();
    }

    public static function Television()
    {
        return new Television();
    }

    public static function toElectronicItemLegacyClass(ElectronicItemInterface $item, InvoiceItemInterface $invoice)
    {
        $legacy = new ElectronicItem();
        $legacy->setType($item->getType());
        $legacy->setWired($item->getType());

        if ($item instanceof Controller) {
            $legacy->setWired($item->isWired());
        } else {
            $legacy->setWired(false);
        }

        $legacy->setPrice($invoice->getPrice());
        return $item;
    }
}
