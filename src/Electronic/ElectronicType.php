<?php
namespace App\Electronic;
use MyCLabs\Enum\Enum;

class ElectronicType extends Enum
{
    const Console = 'Console';
    const Controller = 'Controller';
    const Microwave = 'Microwave';
    const Television = 'Television';
}
