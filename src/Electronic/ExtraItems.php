<?php

namespace App\Electronic;

class ExtraItems extends \ArrayIterator
{
    private $extras;
    private $limit;

    public function __construct($limit)
    {
        parent::__construct([]);
        $this->limit = $limit;
    }

    public function append($value)
    {
        if (!($value instanceof ElectronicItemInterface)) {
            throw new \Exception("Can only add item that extend ElectronicItemInterface");
        }

        if ($this->count() >= $this->limit) {
            throw new \Exception("Cant add extra");
        }

        return parent::append($value);
    }

    public function map(callable $fn)
    {
        $ar = iterator_to_array($this);
        return array_map($fn, $ar);
    }

    public function setLimit($limit)
    {
        if ($this->count() >= $limit) {
            throw new \Exception("Cant change limit when extras.length > limit");
        }

        $this->limit = $limit;
    }

    public function getLimit()
    {
        return $this->limit;
    }
}
