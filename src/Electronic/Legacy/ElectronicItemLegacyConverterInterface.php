<?php
namespace App\Legacy;

use App\Electronic\ElectronicItemInterface;
use App\Invoice\InvoiceItemInterface;

interface ElectronicItemLegacyConverterInterface
{
    public static function toElectronicItemLegacyClass(ElectronicItemInterface $item, InvoiceItemInterface $invoice);
}
