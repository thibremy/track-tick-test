<?php
namespace App\Electronic;
use App\Electronic\ElectronicItemInterface;

class Microwave implements ElectronicItemInterface
{
    public $extras;

    public function __construct()
    {
        $this->extras = new ExtraItems(0);
    }

    public function maxExtras($limit)
    {
        $this->extras->setLimit($limit);
    }

    public function getType()
    {
        return ElectronicType::Microwave();
    }
}
