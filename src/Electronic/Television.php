<?php
namespace App\Electronic;
use App\Electronic\ElectronicItemInterface;

class Television implements ElectronicItemInterface
{
    public $extras;

    public function __construct()
    {
        $this->extras = new ExtraItems(INF);
    }

    public function maxExtras($limit)
    {
        $this->extras->setLimit($limit);
    }

    public function getType()
    {
        return ElectronicType::Television();
    }
}
