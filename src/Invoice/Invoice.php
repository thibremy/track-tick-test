<?php
namespace App\Invoice;

class Invoice {
    private $invoiceItems;

    public function __construct(array $items)
    {
        array_walk($items, function($invoiceItem) {
            $this->addItem($invoiceItem);
            return true;
        });
    }

    public function addItem(InvoiceItemInterface $invoiceItem)
    {
        $this->invoiceItems[] = $invoiceItem;
    }

    public function total()
    {
        $sum = function($total, $item) {
            return $total + $item->getPrice();
        };

        return array_reduce($this->invoiceItems, $sum, 0);
    }

    public function filter(callable $fn) {
        $items = array_filter($this->invoiceItems, $fn);
        return new Invoice($items);
    }

    public function forEach(callable $fn) {
        array_walk($this->invoiceItems, $fn);
        return $this;
    }

    public function sort($fn = NULL) {
        $sortFn = $fn;
        if (is_null($sortFn)) {
            $sortFn = function($a, $b) {
                return ($a->getPrice() * 100) > ($b->getPrice() * 100);
            };
        }
        $sorted = array_merge([], $this->invoiceItems);
        usort($sorted, $sortFn);
        return new Invoice($sorted);
    }
}
