<?php
namespace App\Invoice;

use App\Invoice\InvoiceItemInterface;
use App\Electronic\ElectronicType;

class InvoiceItem implements InvoiceItemInterface
{
    protected $price;
    protected $item;

    public function __construct($item, $price)
    {
        $this->item = $item;
        $this->price = $price;
    }

    public function getLabel() {
        switch ($this->item->getType()) {
            case ElectronicType::Console():
                return "Console";
                break;
            case ElectronicType::Microwave():
                return "Microwave";
                break;
            case ElectronicType::Television():
                return "Television";
                break;
            case ElectronicType::Controller():
                return "Controller";
                break;
        }
    }

    public function getProduct() {
        return $this->item;
    }

    public function getPrice() {
        return $this->price;
    }
}
