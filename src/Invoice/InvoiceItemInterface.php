<?php
namespace App\Invoice;

interface InvoiceItemInterface
{
    public function getProduct();
    public function getPrice();
    public function getLabel();
}
