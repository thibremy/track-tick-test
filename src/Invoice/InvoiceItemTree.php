<?php
namespace App\Invoice;

use App\Invoice\InvoiceItem;

class InvoiceItemTree extends InvoiceItem
{
    public function getPrice() {
        $sum = function ($price, $extra) {
            return $price;
        };

        return array_reduce(iterator_to_array($this->item->extras), $sum, $this->price);
    }
}
