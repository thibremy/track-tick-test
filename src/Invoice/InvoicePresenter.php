<?php
namespace App\Invoice;

use Symfony\Component\Console\Output\OutputInterface;
use App\Invoice\Invoice;
use App\Electronic\Controller;
use App\Electronic\ControllerType;

class InvoicePresenter
{
    private $output;

    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    public function display(Invoice $invoice) {
        $invoice->sort()->foreach(function ($item) {
            $label = $this->labelFor($item);
            $this->output->writeln($label .' ('. $item->getPrice() . '$)');
        });
    }

    private function labelFor($item) {
        return $item->getLabel();
    }
}
