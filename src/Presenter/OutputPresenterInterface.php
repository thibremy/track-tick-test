<?php
namespace App\Invoice;

interface OutputPresenterInterface
{
    public function display($data);
}
